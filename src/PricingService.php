<?php

namespace Zorra\Pricing;

use App\Models\Rate;
use App\Models\Price;

use Zorra\Utils\ApiQuery\PaginateQuery;
use Zorra\Utils\ApiQuery\SortQuery;
use Zorra\Utils\Extentions\Eloquent\MetaPaginator;

use Illuminate\Validation\ValidationException;
use Validator;


class PricingService implements PricingServiceInterface
{
    private $service;

    public function __construct(Rate $rates, Price $prices)
    {
        $this->rates = $rates;
        $this->prices = $prices;

        $this->service = config('app.service');
    }
    
    private function validate($input, $rules) {
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $error = new ValidationException($validator);
            throw $error;
        }
    }

    /*
    ########     ###    ######## ########  ######
    ##     ##   ## ##      ##    ##       ##    ##
    ##     ##  ##   ##     ##    ##       ##
    ########  ##     ##    ##    ######    ######
    ##   ##   #########    ##    ##             ##
    ##    ##  ##     ##    ##    ##       ##    ##
    ##     ## ##     ##    ##    ########  ######
     */

    public function findRate(int $id): Rate
    {
        return $this->rates->findOrFail($id);
    }

    public function getRates(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): MetaPaginator
    {
        $result = $this->rates->where(function ($query) use ($filters) {
            if (!empty($filters['name'])) {
                $query->where('name', 'like', '%' . $filters['name'] . '%');
            }
            if (!empty($filters['currency_id'])) {
                $query->whereIn('currency_id', $filters['currency_id']);
            }
        })->sortByQuery($sort_query)->metaPaginate($paginate_query);
        
        return $result;
    }

    public function createRate($data): Rate
    {        
        $input = $this->rates->filterData($data);

        $rate = $this->rates->create($data);
        return $this->rates->find($rate->id);
    }

    public function updateRate(Rate $rate, array $data): Rate
    {
        $input = $this->rates->filterData($data, true);
        
        $rate->update($input);
        return $rate;
    }

    public function wipeRate(Rate $rate): void
    {
        $this->prices->where(['rate_id' => $rate->id])->delete();
    }

    public function deleteRate(Rate $rate): void
    {
        $rate->delete();
    }

    public function completeRate(Rate $rate, bool $delete_new = true): void
    {
        if ($delete_new) {
            $sign = '>';
            $table_1 = 'p1';
            $table_2 = 'p2';
        } else {
            $sign = '<';
            $table_1 = 'p2';
            $table_2 = 'p1';
        }

        switch ($this->service) {
            case self::SERVICE_VOICE:
                $condition = "p1.code = p2.code";
                break;
            case self::SERVICE_SMS:
                $condition = "p1.mcc = p2.mcc AND p1.mnc = p2.mnc";
                break;
            case self::SERVICE_VIBER:
            case self::SERVICE_HLR:
            case self::SERVICE_WHATSAPP:
                $condition = "p1.mcc = p2.mcc";
                break;
        }

        $deleted = $this->prices->delete(
            "DELETE p1 FROM prices p1, prices p2 WHERE p1.rate_id = ? AND p2.rate_id = ? AND (p1.mcc = p2.mcc AND p1.mnc = p2.mnc) AND p1.id {$sign} p2.id AND (
                ({$table_1}.start >= {$table_2}.start AND {$table_1}.start <= {$table_2}.end) OR ({$table_1}.end >= {$table_2}.start AND {$table_1}.end <= {$table_2}.end)
            )",
            [$rate->id, $rate->id]
        );

        $rate->update(['state' => self::RATE_STATE_READY]);
    }

    public function getRatesRules(bool $update = false)
    {
        return $this->rates->getRules($update);
    }

    /*
    ########  ########  ####  ######  ########  ######
    ##     ## ##     ##  ##  ##    ## ##       ##    ##
    ##     ## ##     ##  ##  ##       ##       ##
    ########  ########   ##  ##       ######    ######
    ##        ##   ##    ##  ##       ##             ##
    ##        ##    ##   ##  ##    ## ##       ##    ##
    ##        ##     ## ####  ######  ########  ######
     */

    public function findPrice(int $id): Price
    {
        return $this->prices->findOrFail($id);
    }

    public function getPrices(Rate $rate, array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): MetaPaginator
    {
        $result = $rate->prices()->where(function ($query) use ($filters) {
            // search name
            if (!empty($filters['search'])) {
                $query->where('name', 'like', '%' . $filters['search'] . '%');
            }
            // mcc
            if (!empty($filters['mcc'])) {
                $query->where(['mcc' => $filters['mcc']]);
            }
            // mnc
            if (!empty($filters['mnc'])) {
                $query->where(['mnc' => $filters['mnc']]);
            }
            // code
            if (!empty($filters['cubrid_error_code()'])) {
                $query->where('code', 'like', $filters['code'] . '%');
            }
        })->sortByQuery($sort_query)->metaPaginate($paginate_query);

        return $result;
    }

    public function addPrices(Rate $rate, array $data): int
    {
        $now = date('Y-m-d H:i:s');

        $rate->upate(['state' => self::RATE_STATE_PROCESSING]);

        switch ($this->service) {
            case self::SERVICE_SMS:
                $mandatory = ['mcc', 'mnc', 'price', 'start', 'end'];
                break;
            case self::SERVICE_VOICE:
                $mandatory = ['code', 'price', 'start', 'end'];
                break;
            case self::SERVICE_VIBER:
            case self::SERVICE_HLR:
            case self::SERVICE_WHATSAPP:
                $mandatory = ['mcc', 'price', 'start', 'end'];
                break;
        }

        $batch = [];

        foreach ($data as $row) {
            $skip = false;
            foreach ($mandatory as $key) {
                if (!isset($row[$key])) {
                    $skip = true;
                    break;
                }
            }

            if (!$skip) {
                $tmp = Price::filterData($row);
                $tmp['created_at'] = $now;
                $tmp['rate_id'] = $rate->id;
                $batch[] = $tmp;
            }
        }

        $this->prices->insert($batch);

        return count($batch);
    }

    public function updatePrice(Price $price, array $data): Price
    {
        $input = $this->prices->filterData($data, true);
        
        $price->update($input);
        return $price;
    }

    public function deletePrice(Price $price): void
    {
        $price->delete();
    }

    public function getPricesRules(bool $update = false)
    {
        return $this->prices->getRules($update);
    }

}
