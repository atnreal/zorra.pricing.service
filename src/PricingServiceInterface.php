<?php

namespace Zorra\Pricing;

use Zorra\Utils\ApiQuery\PaginateQuery;
use Zorra\Utils\ApiQuery\SortQuery;
use Zorra\Utils\Extentions\Eloquent\MetaPaginator;

use App\Models\Rate;
use App\Models\Price;

interface PricingServiceInterface
{
    const SERVICE_SMS = 'SMS';
    const SERVICE_HLR = 'HLR';
    const SERVICE_VIBER = 'VIBER';
    const SERVICE_WHATSAPP = 'WHATSAPP';
    const SERVICE_VOICE = 'VOICE';

    const RATE_STATE_READY = 'ready';
    const RATE_STATE_PROCESSING = 'processing';
    const RATE_STATE_SYNC = 'sync';
    const RATE_STATE_ERROR = 'error';

    // rates

    public function findRate(int $id): Rate;

    public function getRates(array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): MetaPaginator;

    public function createRate(array $data): Rate;

    public function wipeRate(Rate $rate): void;

    public function deleteRate(Rate $rate): void;

    public function updateRate(Rate $rate, array $data): Rate;

    public function completeRate(Rate $rate, bool $delete_new = true): void;

    public function getRatesRules(bool $update = false);

    // prices

    public function findPrice(int $id): Price;

    public function getPrices(Rate $rate, array $filters, PaginateQuery $paginate_query, SortQuery $sort_query): MetaPaginator;

    public function addPrices(Rate $rate, array $data): int;

    public function updatePrice(Price $price, array $data): Price;

    public function deletePrice(Price $price): void;

    public function getPricesRules(bool $update = false);

}
