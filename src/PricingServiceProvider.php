<?php

namespace Zorra\Pricing;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\ServiceProvider;

class PricingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $service = config('app.service') ?? null;
        if (!in_array($service, ['HLR', 'SMS', 'VOICE', 'VIBER', 'WHATSAPP'])) {
            throw new \Exception('Wrong app.service config data');
        }

        $path = __DIR__ . "/migrations/{$service}";
        $this->loadMigrationsFrom($path);
    }
}
