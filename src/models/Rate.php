<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ['name', 'currency_id'];

    protected $immutable = ['id', 'currency_id', 'state', 'created_at', 'updated_at'];
    protected $rules = [
        'name' => 'required|string|max:128',
        'currency_id' => 'required|integer',
    ];

    public function getRules($update = false) {
        if ($update) {
            $res = array_diff_key($this->rules, array_flip($this->immutable));
            return $res;
        } else {
            return $this->rules;
        }
    }

    public function filterData($data, $update = false) {
        if ($update) {
            $data = array_diff_key($data, array_flip($this->immutable));
        }
        return $data;
    }

    // relations

    public function prices() {
        return $this->hasMany('App\Models\Price');
    }

}
