<?php

namespace Zorra\Pricing\Test;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

use Exception;
use App\Models\Price;
use App\Models\Rate;

use Zorra\Pricing\PricingService;
use Zorra\Utils\Extentions\Eloquent\MetaPaginator;

class PricingServiceTest extends TestCase {

    private $service;
    private $rates;
    private $prices;

    public function getRatesMock() {
        $rates = $this->getMockBuilder(Rate::class)->setMethods(['find', 'update', 'create', 'insert', 'delete', 'where', 'sortByQuery', 'metaPaginate'])->getMock();
        // $rates = $this->getMockBuilder(Rate::class)->getMock();
        return $rates;
    }

    public function getPricesMock() {
        $prices = $this->getMockBuilder(Price::class)->setMethods(['find', 'update', 'create', 'insert', 'delete', 'where', 'sortByQuery', 'metaPaginate'])->getMock();
        // $prices = $this->getMockBuilder(Price::class)->getMock();
        return $prices;
    }

}